for i in 1..500 do
    if i % 15 == 0
        puts 'FizzBuzz'
    elsif i % 3 == 0 
        puts 'Fizz'
    elsif i % 5 == 0
        puts 'Buzz'
    else 
        puts i
    end
end

for i in 1..500 do
    puts i % 15 == 0 ? 'FizzBuzz' : i % 5 == 0 ? 'Buzz' : i % 3 == 0 ? 'Fizz' : i
end